/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication13;

/**
 *
 * @author autonome
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Myconnection {
    //java data base connector =jdbc
    private static Myconnection instanceconnection ;
    private String url="jdbc:mysql://localhost:3306/happy_olds";
    private String login="root";
    private String mdp="" ;
    private   Connection conx;
    
    private Myconnection()
    {
        try {
            conx= DriverManager.getConnection(url,login,mdp);
            System.out.println("connection est établie");
        } catch (SQLException ex) {
            Logger.getLogger(Myconnection.class.getName()).log(Level.SEVERE, null, ex);
        }
  
    }
    
    
    public static Myconnection getInsCon(){
       if (instanceconnection==null){
            instanceconnection = new Myconnection();
        } 
        return instanceconnection ;
    }
    
    
   public  Connection getcnx(){
       return conx; 
   }
   
   
}
