/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import Entities.Agee;
import Entities.User;
import javaapplication13.Myconnection;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.lang.Integer.parseInt;
//import static javax.xml.bind.DatatypeConverter.parseDate;

/**
 *
 * @author autonome
 */
public class Admin_service implements IService<User> {
    
    Myconnection Mc=Myconnection.getInsCon();
    private Connection connection;
    private Statement st ;
    private ResultSet rs ;
    private PreparedStatement pst ;
    public Admin_service() {
        connection=Myconnection.getInsCon().getcnx();
    }


    
        public void insertPStatement(User U) {
        try {
            rs= null ;
            String requete =
                    "INSERT INTO user(nom,prenom,sexe,mail,region,adresse,pwd,cin,telephone,type) VALUES(?,?,?,?,?,?,?,?,?,?)";

            String requete2 =
                    "INSERT INTO planning_traitement_medical(id_Agee) VALUES(?)";

            String requete3 =
                    "INSERT INTO planning_nutrition(id_Agee) VALUES(?)";

            String requete4 =
                    "INSERT INTO planning_visite_medicale(id_Agee) VALUES(?)";


//            rs.getInt(1);
//            while ( rs.next() )
//            {
//
//                insert_id = rs.getInt("i");
//
//            }



            pst =connection.prepareStatement(requete);

            pst.setString(1, U.getNom());
            pst.setString(2, U.getPrenom());
            pst.setString(3, U.getSexe());
            pst.setString(4, U.getMail());
            pst.setString(5, U.getRegion());
            pst.setString(6, U.getAdresse());
            pst.setString(7, U.getPwd());

            pst.setInt(8, U.getCin());
            pst.setInt(9, U.getTelephone());

            pst.setString(10, "agee");


            System.out.println(U.getNom());
            System.out.println(U.getCin());


            pst.executeUpdate();



            String requete0=
                    "select MAX(id) as i from user";
            st =connection.createStatement();
            rs = st.executeQuery(requete0);

            int insert_id = 0;
            if(rs.next()){
                insert_id=rs.getInt("i");
            }



            System.out.println("Personne ajoutée");
            System.out.println("__________________________________ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo_____________________________________________"+insert_id);
            System.out.println("__________________________________ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo_____________________________________________"+insert_id);



            pst =connection.prepareStatement(requete2);
            pst.setInt(1, insert_id);
            pst.executeUpdate();


            pst =connection.prepareStatement(requete3);
            pst.setInt(1, insert_id);
            pst.executeUpdate();


            pst =connection.prepareStatement(requete4);
            pst.setInt(1, insert_id);
            pst.executeUpdate();





            System.out.println("Personne ajoutée");
            System.out.println("__________________________________ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo_____________________________________________"+rs.getInt("i"));
        }
        catch (SQLException ex){
                System.out.println(ex.getMessage());
        }    
    }


     

    @Override
    public void delete(User P) {
        try {
        String requete =
                "DELETE FROM user where user.cin='"
                +P.getCin()+"'";
                st =connection.createStatement();
                st.executeUpdate(requete);
                System.out.println("user supprimée____ "+P.toString());
        }
        catch (SQLException ex){
            System.out.println(ex.getMessage());
        } 
    }

    @Override
    public void update(User U) {
        try {
            String requete =
                    "UPDATE user SET nom = ? , prenom = ? , sexe = ?, mail = ? , region = ? , adresse = ? , pwd = ? , cin = ? , telephone = ? "
                            + "where cin = ? or mail = ?";

            PreparedStatement st =connection.prepareStatement(requete);

            st.setString(1, U.getNom());
            st.setString(2, U.getPrenom());
            st.setString(3, U.getSexe());
            st.setString(4, U.getMail());
            st.setString(5, U.getRegion());
            st.setString(6, U.getAdresse());
            st.setString(7, U.getPwd());

            st.setInt(8, U.getCin());
            st.setInt(9, U.getTelephone());

            st.setInt(10, U.getCin());
            st.setString(11, U.getMail());

            st.executeUpdate();
            System.out.println("agee modifier");


        }
        catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
    }

    public void updateNom(int id , String nom ) {
        try {
            String requete =
                    "UPDATE user SET nom = ?  "
                            + "where id = ? ";

            PreparedStatement st =connection.prepareStatement(requete);

            st.setString(1,  nom);
            st.setInt(2, id);


            st.executeUpdate();
            System.out.println("agee modifier");


        }
        catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
    }


    public void updatePrenom(int id , String prenom ) {
        try {
            String requete =
                    "UPDATE user SET prenom = ?  "
                            + "where id = ? ";

            PreparedStatement st =connection.prepareStatement(requete);

            st.setString(1,  prenom);
            st.setInt(2, id);


            st.executeUpdate();
            System.out.println("agee modifier");


        }
        catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
    }

    public void updateRib(int id , String rib ) {
        try {
            String requete =
                    "UPDATE user SET rib = ?  "
                            + "where id = ? ";

            PreparedStatement st =connection.prepareStatement(requete);

            st.setString(1,  rib);
            st.setInt(2, id);


            st.executeUpdate();
            System.out.println("agee modifier");


        }
        catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
    }

    public void updateDate(int id , java.util.Date date ) {
        try {
            String requete =
                    "UPDATE user SET date = ?  "
                            + "where id = ? ";

            PreparedStatement st =connection.prepareStatement(requete);

            st.setDate(1, (Date) date);
            st.setInt(2, id);


            st.executeUpdate();
            System.out.println("agee modifier");


        }
        catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
    }

    public void updateCin(int id , int cin ) {
        try {
            String requete =
                    "UPDATE user SET cin = ?  "
                            + "where id = ? ";

            PreparedStatement st =connection.prepareStatement(requete);

            st.setInt(1,  cin);
            st.setInt(2, id);


            st.executeUpdate();
            System.out.println("agee modifier");


        }
        catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
    }

    public void updateTelephone(int id , int telephone ) {
        try {
            String requete =
                    "UPDATE user SET telephone = ?  "
                            + "where id = ? ";

            PreparedStatement st =connection.prepareStatement(requete);

            st.setInt(1,  telephone);
            st.setInt(2, id);


            st.executeUpdate();
            System.out.println("agee modifier");


        }
        catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
    }

    public void updateAdresse(int id , String adresse ) {
        try {
            String requete =
                    "UPDATE user SET adresse = ?  "
                            + "where id = ? ";

            PreparedStatement st =connection.prepareStatement(requete);

            st.setString(1,  adresse);
            st.setInt(2, id);


            st.executeUpdate();
            System.out.println("agee modifier");


        }
        catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
    }
    
    
    
        public ObservableList<User> getAll() {
            ObservableList<User> list = FXCollections.observableArrayList();
//            String requete = "select * from user where user.type= 'agee'";
            String requete = "select * from user where user.roles= 'a:1:{i:0;s:9:\"ROLE_AGEE\";}'";
            try {
                st =connection.createStatement();
                rs= st.executeQuery(requete);
                while(rs.next()){
                    System.out.println("______________________"+rs.getString("rib"));

//                    list.add(new User(rs.getString("nom"), rs.getString("prenom")));
//                    list.add(new Agee(rs.getInt("id"),rs.getString("nom"), rs.getString("prenom"), rs.getDate(5) , rs.getString("sexe"), rs.getString("mail"), rs.getString("region"), rs.getString("adresse"), rs.getString("pwd"), rs.getInt(2), rs.getInt(11) , rs.getString("rib")                   ));
                    list.add(new Agee(rs.getInt("id"),
                            rs.getString("nom"),
                            rs.getString("prenom"),
                            rs.getDate(16) ,
                            rs.getString("sexe"),
                            rs.getString("email"),
                            rs.getString("region"),
                            rs.getString("adresse"),
                            rs.getString("password"),
                            rs.getInt(15),
                            rs.getInt(20) ,
                            rs.getString("rib")
                    ));
                }
            } catch (SQLException ex) {
                Logger.getLogger(Admin_service.class.getName()).log(Level.SEVERE, null, ex);
            }
            return list ;
    }

    public ObservableList<User> getAllUser() {
        ObservableList<User> list = FXCollections.observableArrayList();
        String requete = "select * from user ";
        try {
            st =connection.createStatement();
            rs= st.executeQuery(requete);
            while(rs.next()){
                System.out.println("______________________"+rs.getString("rib"));

//                    list.add(new User(rs.getString("nom"), rs.getString("prenom")));
                list.add(new Agee(rs.getInt("id"),
                        rs.getString("nom"),
                        rs.getString("prenom"),
                        rs.getDate(16) ,
                        rs.getString("sexe"),
                        rs.getString("email"),
                        rs.getString("region"),
                        rs.getString("adresse"),
                        rs.getString("password"),
                        rs.getInt(15),
                        rs.getInt(20) ,
                        rs.getString("rib")
                ));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Admin_service.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list ;
    }

    public ObservableList<User> getAllOldSiter() {
        ObservableList<User> list = FXCollections.observableArrayList();
        String requete = "select * from user where user.roles= 'a:1:{i:0;s:9:\\\"ROLE_OLDSITTER\\\";}'";
        try {
            st =connection.createStatement();
            rs= st.executeQuery(requete);
            while(rs.next()){
                System.out.println("_____88888888___________88888888_________"+rs.getString("nom"));

//                    list.add(new User(rs.getString("nom"), rs.getString("prenom")));
                list.add(new Agee(rs.getInt("id"),rs.getString("nom"), rs.getString("prenom"), rs.getDate(5) , rs.getString("sexe"), rs.getString("mail"), rs.getString("region"), rs.getString("adresse"), rs.getString("pwd"), rs.getInt(2), rs.getInt(11) , rs.getString("rib")                   ));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Admin_service.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list ;
    }
        
        
      
        
        
        
        
        
        
        
        

        public boolean isInteger(String string) {
            try {
                Integer.valueOf(string);
                return true;
            } catch (NumberFormatException e) {
                return false;
            }
        }
        
        public boolean isDate(String string) {
            try {
                Date.valueOf(string);
                return true;
            } catch (Exception e) {
                return false;
            }
        }


        
        
        
        public List<User> getByString(String s) throws ParseException {
            
            boolean isInt = false;
            boolean isDt = false;
            
            int sInt = -1 ;
            Date sDt = new Date(00,00,00) ;
            Date sqlStartDate = null ;
            
            
            isInt = isInteger(s);
            if(isInt){
                sInt = parseInt(s);
            }
            
            isDt = isDate(s);
            if(isDt){
                
                SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
                java.util.Date date = sdf1.parse(s);
                 sqlStartDate = new Date(date.getTime());
                
//                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
//                SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
//                sDt =  Date.parse(s);
            }
            
            
            List<User> list = new ArrayList<>();
            String requete = "select * from user "
                    + "where ( user.nom = '"+s
                        + "' or user.prenom = '"+s
                        + "' or user.sexe = '"+s
                        + "' or user.mail = '"+s
                        + "' or user.region = '"+s
                        + "' or user.adresse = '"+s
                        + "' or user.cin = '"+sInt
                        + "' or user.telephone = '"+sInt
                        + "' or user.dateNaissance = '"+sqlStartDate
                        + "' or user.nom = '"+s
                        + "' ) and user.type = 'agee'";

            try {
                st =connection.createStatement();
                rs= st.executeQuery(requete);
                while(rs.next()){
                    System.out.println(""+rs.getString("rib"));
                    list.add(new Agee(rs.getString("nom"), rs.getString("prenom"),rs.getDate(s) , rs.getString("sexe"), rs.getString("mail"), rs.getString("region"), rs.getString("adresse"), rs.getString("pwd"), rs.getInt(11), rs.getInt(2) , rs.getString("rib") ));
                }
            } catch (SQLException ex) {
                Logger.getLogger(Admin_service.class.getName()).log(Level.SEVERE, null, ex);
            }
            return list ;
    }


    public int nbrAgee() throws SQLException {
        ResultSet rs = null;
        try {
            String requete =
                    "SELECT count (*) as total from user where type= 'agee'";
            st =connection.createStatement();
            rs = st.executeQuery(requete);
            System.out.println("Count effectuée");
        }
        catch (SQLException ex){
            System.out.println(ex.getMessage());
        }

        return rs.getInt("total");
    }
        
        
    
        



   

   

    
    

}
