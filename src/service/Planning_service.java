package service;

import Entities.Planning;
import javaapplication13.Myconnection;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;




public class Planning_service {

    Myconnection Mc=Myconnection.getInsCon();
    private Connection connection;
    private Statement st ;
    private PreparedStatement pst ;
    private ResultSet rs ;


    public Planning_service(){
        connection=Myconnection.getInsCon().getcnx();
    }


    public ObservableList<Planning> getAll() {
        System.out.println("__________________________________getAll planning service_____________________________");
        System.out.println("__________________________________getAll planning service_____________________________");
        System.out.println("__________________________________getAll planning service_____________________________");
        System.out.println("__________________________________getAll planning service_____________________________");
        System.out.println("__________________________________getAll planning service_____________________________");
        System.out.println("__________________________________getAll planning service_____________________________");
        ObservableList<Planning> list = FXCollections.observableArrayList();
        String requete = "select distinct * from planning_nutrition , planning_traitement_medical , planning_visite_medicale , user" +
                " where user.id = planning_nutrition.id_agee " +
                "     AND user.id = planning_traitement_medical.id_agee" +
                "     And user.id = planning_visite_medicale.id_agee";
        try {
            System.out.println("create statement");
            st =connection.createStatement();
            System.out.println("statement creé");
            System.out.println("query excution");
            rs= st.executeQuery(requete);
            System.out.println("excute query done");
            System.out.println(rs.toString());
            while(rs.next()){
                System.out.println("____mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm____");
                System.out.println("____mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm____"+rs.getInt("id_Agee"));
                System.out.println("____mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm____"+rs.getString("nom"));
//                String requete2 = "select DISTINCT from user where user.id= '"+rs.getInt("id_Agee")+"' ";
//                ResultSet rs2 = st.executeQuery(requete2);
//    public Planning(int id_pn, int id_ptm, int id_pvm ,  int id_Agee,                                                                  String nomAgee , String prenomAgee , String desc_nutrition , String medicament ,                                               String desc_traitement, int dureeTraitement , String petitDejeuner , String dejeuner , String diner , String date_rdv) {
                    list.add(new Planning(rs.getInt("id_pn"),rs.getInt("id_ptm"),rs.getInt("id_vm"),rs.getInt("id_agee"),rs.getString("nom"),rs.getString("prenom"),rs.getString("nutrition_desc"), rs.getString("nom_medicament"),rs.getString("traitement_desc"), rs.getInt("duree_en_jour_de_traitement"),rs.getString("petitDejeuner"),rs.getString("dejeuner"),rs.getString("diner"),rs.getString("date_rdv") ));
                System.out.println("____mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm____"+list.get(0).ToString());


            }
            System.out.println(list.toString());
        } catch (SQLException ex) {
            System.out.println("catch get all exception planning service ______________________________");
            Logger.getLogger(Admin_service.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list ;
    }


    public void delete(Planning P) {
        try {
            String requete =
                    "DELETE FROM from planning_nutrition , planning_traitement_medical , planning_visite_medicale , user " +
                            " where user.id = planning_nutrition.id_Agee " +
                            "     AND user.id = planning_traitement_medical.id_Agee" +
                            "     And user.id = planning_visite_medicale.id_Agee" +
                            "     AND user.id='"
                            +P.getid_Agee()+"'";

            st =connection.createStatement();
            st.executeUpdate(requete);
            System.out.println("user supprimée____ "+P.toString());
        }
        catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
    }


    public void updateDescNutrition(int id, String desc) {
        try {

            String requete =
                    "UPDATE planning_nutrition SET nutrition_desc = ?  "
                            + "where id_Agee = ? ";

            pst =connection.prepareStatement(requete);


            pst.setString(1,  desc);
            pst.setInt(2, id);


            pst.executeUpdate();
            System.out.println("agee modifier");
        }
        catch (SQLException ex){
            System.out.println(ex.getMessage());
        }


    }

    public void updateNomMedicament(int id, String medicament) {
        try {

            System.out.println("id________: "+id);
            System.out.println("nom medicament________: "+medicament);
            String requete =
                    "UPDATE planning_traitement_medical SET nom_medicament = ?  "
                            + "where id_Agee = ? ";

            pst =connection.prepareStatement(requete);


            pst.setString(1,  medicament);
            pst.setInt(2, id);


            pst.executeUpdate();
            System.out.println("nom medicament modifier");
        }
        catch (SQLException ex){
            System.out.println(ex.getMessage());
        }


    }

    public void updateTraitement(int id, String traitement) {
        try {

            String requete =
                    "UPDATE planning_traitement_medical SET traitement_desc = ?  "
                            + "where id_Agee = ? ";

            pst =connection.prepareStatement(requete);


            pst.setString(1,  traitement);
            pst.setInt(2, id);


            pst.executeUpdate();
        }
        catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
    }

    public void updatePetitDejeuner(int id, String avantApresNon) {
        try {

            String requete =
                    "UPDATE planning_traitement_medical SET petitDejeuner = ?  "
                            + "where id_Agee = ? ";

            pst =connection.prepareStatement(requete);


            pst.setString(1,  avantApresNon);
            pst.setInt(2, id);


            pst.executeUpdate();
        }
        catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
    }

    public void updatedejeuner(int id, String avantApresNon) {
        try {

            String requete =
                    "UPDATE planning_traitement_medical SET dejeuner = ?  "
                            + "where id_Agee = ? ";

            pst =connection.prepareStatement(requete);


            pst.setString(1,  avantApresNon);
            pst.setInt(2, id);


            pst.executeUpdate();
        }
        catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
    }

    public void updateDiner(int id, String avantApresNon) {
        try {

            String requete =
                    "UPDATE planning_traitement_medical SET diner = ?  "
                            + "where id_Agee = ? ";

            pst =connection.prepareStatement(requete);


            pst.setString(1,  avantApresNon);
            pst.setInt(2, id);


            pst.executeUpdate();
        }
        catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
    }
}



