/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import Entities.Planning;
import Entities.PlanningNutrition;
import Entities.User;
import javaapplication13.Myconnection;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.lang.Integer.parseInt;

/**
 *
 * @author autonome
 */
public class PlanningNutrition_service implements IService{
    Myconnection Mc=Myconnection.getInsCon();
    private Connection connection;
    private Statement st ;
    private ResultSet rs ;

    public PlanningNutrition_service() {
        connection=Myconnection.getInsCon().getcnx();
    }
    
    

    public void insertPStatement(Planning P, User U) {
        try {
            User_service US = new User_service();
            int id_Agee = US.getIdFormDB(U);
            String requete =
                    "INSERT INTO planning_nutrition ( id_Agee , nutrition_desc ) VALUES(?,?)";
            PreparedStatement st =connection.prepareStatement(requete);
            st.setInt(1, id_Agee);
            st.setString(2, ((PlanningNutrition) P).getDesc());
            st.executeUpdate();
            System.out.println("desc ajoutee");
        }
        catch (SQLException ex){
                System.out.println(ex.getMessage());
        }

    }

    
    public void delete( User U) {
        User_service US = new User_service();
        int id_Agee = US.getIdFormDB(U);
        try {
        String requete =
                "DELETE FROM planning_nutrition where planning_nutrition.id_Agee='"
                +id_Agee+" ' ";
                st =connection.createStatement();
                st.executeUpdate(requete);
                System.out.println("planning nutrition supprimée");
        }
        catch (SQLException ex){
            System.out.println(ex.getMessage());
        } 
    }


    public void update(User U, PlanningNutrition PN) {
        User_service US = new User_service();
        int id_Agee = US.getIdFormDB(U);
        try {
        String requete =
                "UPDATE  planning_nutrition set nutrition_desc = ?"
                + " where id_Agee = ? ";
                PreparedStatement st =connection.prepareStatement(requete);
            
                st.setString(1, PN.getDesc());
                st.setInt(2, id_Agee );

                st.executeUpdate();
                System.out.println("planning nutrition modifié");
        }
        catch (SQLException ex){
            System.out.println(ex.getMessage());
        }     
    }
    
    
    public List<Planning> getAll() {
            List<Planning> list = new ArrayList<>();
            String requete = "select * from planning_nutrition ";
            try {
                st =connection.createStatement();
                rs= st.executeQuery(requete);
                while(rs.next()){
                    list.add(new PlanningNutrition(rs.getInt("id_pn"), rs.getInt("id_Agee"), rs.getString("nutrition_desc")));
                }
            } catch (SQLException ex) {
                Logger.getLogger(Admin_service.class.getName()).log(Level.SEVERE, null, ex);
            }
            return list ;
    }
    
    
    
    public boolean isInteger(String string) {
            try {
                Integer.valueOf(string);
                return true;
            } catch (NumberFormatException e) {
                return false;
            }
        }
        
        public boolean isDate(String string) {
            try {
                Date.valueOf(string);
                return true;
            } catch (Exception e) {
                return false;
            }
        }
    
    
     public List<Planning> getByString(String s) throws ParseException {
            
            boolean isInt = false;
            boolean isDt = false;
            
            int sInt = -1 ;
            Date sDt = new Date(00,00,00) ;
            Date sqlStartDate = null ;
            
            
            isInt = isInteger(s);
            if(isInt){
                sInt = parseInt(s);
            }
            
            isDt = isDate(s);
            if(isDt){
                
                SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
                java.util.Date date = sdf1.parse(s);
                 sqlStartDate = new Date(date.getTime());
                
//                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
//                SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
//                sDt =  Date.parse(s);
            }
       
            List<Planning> list = new ArrayList<>();
            String requete = "select * from planning_nutrition p , user u "
                    + " where ( "
                    + " u.type = 'agee' AND u.id = p.id_Agee "
                    + "  AND ( p.nutrition_desc = '"+s
                        + "'  OR  u.nom = '"+s
                        + "'  OR u.prenom = '"+s
                        + "'  OR u.sexe = '"+s
                        + "'  OR u.mail = '"+s
                        + "'  OR u.region = '"+s
                        + "'  OR u.adresse = '"+s
                        + "'  OR u.cin = '"+sInt
                        + "'  OR u.telephone = '"+sInt
                        + "'  OR u.dateNaissance = '"+sqlStartDate
                        + "'  OR u.nom = '"+s+"' )) ";
                    

            try {
                st =connection.createStatement();
                rs= st.executeQuery(requete);
                while(rs.next()){
                    list.add(new PlanningNutrition(rs.getInt("id_pn"), rs.getInt("id_Agee"), rs.getString("nutrition_desc")));
                }
            } catch (SQLException ex) {
                Logger.getLogger(Admin_service.class.getName()).log(Level.SEVERE, null, ex);
            }
            return list ;
    }
    

    @Override
    public void insertPStatement(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
