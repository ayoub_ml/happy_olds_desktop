package home;


import Entities.Agee;
import Entities.Planning;
import Entities.User;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.binding.IntegerBinding;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.converter.IntegerStringConverter;
import service.Admin_service;
import service.Planning_service;

import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Controller implements Initializable {


    @FXML
    private Button btn_pdf;

    @FXML
    private VBox pnItems = null;
    @FXML
    private Button btnOverview;

    @FXML
    private Button btnOrders;

    @FXML
    private Button btnCustomers;

    @FXML
    private Button btnMenus;

    @FXML
    private Button btnPackages;

    @FXML
    private Button btnSettings;

    @FXML
    private Button btnSignout;

    @FXML
    private Pane pnlCustomer;

    @FXML
    private Pane pnlOrders;

    @FXML
    private Pane pnlOverview;

    @FXML
    private Pane pnlMenus;




    @FXML
    private Label totalUsers;

    @FXML
    private Label totalAgee;

    @FXML
    private Label totalOldSitter;





    @FXML
    private TableView<User> tableUser;

    @FXML
    private TableColumn<User, Integer> col_id;

    @FXML
    private TableColumn<User, String> col_nom;

    @FXML
    private TableColumn<User, String> col_prenom;


    @FXML
    private TableColumn<User, String> col_date;

    @FXML
    private TableColumn<User, Integer> col_cin;

    @FXML
    private TableColumn<User, Integer> col_tel;

    @FXML
    private TableColumn<User, String> col_adresse;

    @FXML
    private TableColumn<User, String> col_rib;

    @FXML
    private TableColumn<User, String> col_supprimer;

    @FXML
    private TableColumn<Planning, String> col_supprimer2;


//    ObservableList<User> oblist = FXCollections.observableArrayList();
    ObservableList<User> oblist ;
    Admin_service as ;


    ObservableList<Planning> oblist2 ;
    Planning_service ps ;







    @FXML
    private JFXTextField tf_rib;

    @FXML
    private JFXTextField tf_nom;

    @FXML
    private JFXTextField tf_cin;

    @FXML
    private JFXTextField tf_prenom;

    @FXML
    private JFXTextField tf_adresse;

    @FXML
    private JFXTextField tf_region;

    @FXML
    private JFXTextField tf_mail;

    @FXML
    private JFXTextField tf_telephone;

    @FXML
    private DatePicker dp_date;



    @FXML
    private JFXRadioButton rd_homme;

    @FXML
    private JFXRadioButton rd_femme;



    @FXML
    private JFXButton btn_ajouter;





    // gestion de planning
    @FXML
    private TableView<Planning> tableUser1;

    @FXML
    private TableColumn< Planning, String > col_desc_nutrition;

    @FXML
    private TableColumn<Planning, Integer> col_dureeTraitement;

    @FXML
    private TableColumn<Planning, String> col_dejeuner;

    @FXML
    private VBox pnItems1;

    @FXML
    private TableColumn<Planning, String> col_nom1;

    @FXML
    private TableColumn<Planning, String> col_petitDejeuner;

    @FXML
    private TableColumn<Planning, String> col_desc_traitement;

    @FXML
    private TableColumn<Planning, String> col_prenom1;

    @FXML
    private TableColumn<Planning, Integer> col_id_Agee;

    @FXML
    private TableColumn<Planning, String> col_diner;

    @FXML
    private TableColumn<Planning, String> col_medicament;





    boolean sup1 = true ;
    boolean sup2 = false ;




    @Override
    public void initialize(URL location, ResourceBundle resources) {

        initTable();


        if (sup1){
            pnlOverview.setStyle("-fx-background-color : #BBD2E1");
            pnlOverview.toFront();
            sup1 =false ;
        }

        if (sup2){
            pnlOrders.setStyle("-fx-background-color : #BBD2E1");
            pnlOrders.toFront();
            sup2 = false ;
        }







        as = new Admin_service() ;

        oblist = as.getAll();

        tableUser.setItems(oblist);



        ps = new Planning_service();
        oblist2 = ps.getAll();
        tableUser1.setItems(oblist2);





        col_supprimer = new TableColumn("Action");
        col_supprimer2 = new TableColumn("Action");


        IntegerBinding sizeProperty = Bindings.size(oblist);
        BooleanBinding multipleElemsProperty = new BooleanBinding() {
            @Override protected boolean computeValue() {
                return oblist.size() > 1;
            }
        };

        System.out.println("_'__'_'_'_'_'_'_"+sizeProperty);

//        totalAgee = new Label();


        totalAgee.setText(""+oblist.size());
        totalUsers.setText(""+as.getAllUser().size());
        totalOldSitter.setText(""+as.getAllOldSiter().size());
//        try {
//            totalAgee.setText(""+as.nbrAgee());
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
        System.out.println("000000000000000000000_______________________________________________"+totalAgee.getText());
        System.out.println("000000000000000000000_______________________________________________"+totalAgee.getText());
        System.out.println("000000000000000000000_______________________________________________"+totalAgee.getText());
        System.out.println("000000000000000000000_______________________________________________"+totalAgee.getText());
        System.out.println("000000000000000000000_______________________________________________"+totalAgee.getText());





        // radio button _____________ajout agée________________
        ajoutChampRequired();

        ToggleGroup group = new ToggleGroup();
        rd_homme.setToggleGroup(group);
        rd_femme.setToggleGroup(group);
        rd_homme.setSelected(true);




//        Node[] nodes = new Node[10];
//        for (int i = 0; i < nodes.length; i++) {
//            try {
//
//                final int j = i;
//                nodes[i] = FXMLLoader.load(getClass().getResource("Item.fxml"));
//
//                //give the items some effect
//
//                nodes[i].setOnMouseEntered(event -> {
//                    nodes[j].setStyle("-fx-background-color : #0A0E3F");
//                });
//                nodes[i].setOnMouseExited(event -> {
//                    nodes[j].setStyle("-fx-background-color : #02030A");
//                });
//                pnItems.getChildren().add(nodes[i]);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }

    }

    private void ajoutChampRequired() {
//        RequiredFieldValidator validator = new RequiredFieldValidator();
//
//        tf_nom.getValidators().add(validator);
//        validator.setMessage("Entrer le Nom");
//
//        tf_nom.focusedProperty().addListener(new ChangeListener<Boolean>(){
//
//            public void changed (ObservableValue<? extends Boolean> observable, Boolean oldValue , Boolean newValue){
//                if(!newValue){
//                    tf_nom.validate();
//                }
//            }
//        });
//
//        try {
//            Image icn = new Image(new FileInputStream("/home/autonome/Téléchargements/RestaurantMgtSampleUI-master/src/images"));
//            validator.setIcon(icn);
//        }catch (Exception e){
//            e.printStackTrace();
//        }

    }


    private void initTable(){

        initCols();

    }

    private void initCols(){

        col_id.setCellValueFactory(new PropertyValueFactory<User, Integer>("id"));
        col_nom.setCellValueFactory(new PropertyValueFactory<User, String> ("nom"));
        col_prenom.setCellValueFactory(new PropertyValueFactory<User, String> ("prenom"));
        col_rib.setCellValueFactory(new PropertyValueFactory<User, String> ("rib"));
        col_date.setCellValueFactory(new PropertyValueFactory<User, String> ("DateNaissance"));
        col_cin.setCellValueFactory(new PropertyValueFactory<User, Integer>("cin"));
        col_tel.setCellValueFactory(new PropertyValueFactory<User, Integer>("telephone"));
        col_adresse.setCellValueFactory(new PropertyValueFactory<User, String> ("adresse"));
//        col_supprimer.setCellValueFactory(new PropertyValueFactory<User, Button> ("supprimer"));

        col_supprimer = new TableColumn("Supprimer");
        //col_supprimer.setCellValueFactory(new PropertyValueFactory<User, String>("supprimer"));

        Callback<TableColumn<User, String>, TableCell<User, String>> cellFactory
                = //
                new Callback<TableColumn<User, String>, TableCell<User, String>>() {
                    @Override
                    public TableCell call(final TableColumn<User, String> param) {
                        final TableCell<User, String> cell = new TableCell<User, String>() {

                            final Button btn = new Button("supprimer");

                            @Override
                            public void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                if (empty) {
                                    setGraphic(null);
                                    setText(null);
                                } else {
                                    btn.setOnAction(event -> {
                                        User person = getTableView().getItems().get(getIndex());
                                        System.out.println(person.getNom()
                                                + "   " + person.getPrenom());
                                        as.delete(person);
                                        sup1= true ;
                                        sup2= false ;

                                        Parent root;
                                        try {
                                            root= FXMLLoader.load(getClass().getResource("Home2.fxml"));
                                            btn.getScene().setRoot(root);
                                        } catch (IOException ex) {
                                            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                    });
                                    setGraphic(btn);
                                    setText(null);
                                }
                            }
                        };
                        return cell;
                    }
                };

        col_supprimer.setCellFactory(cellFactory);
        tableUser.getColumns().add(col_supprimer);








        // table Gestion Planning

        col_desc_nutrition.setCellValueFactory(new PropertyValueFactory<Planning, String>("desc_nutrition"));
        col_dureeTraitement.setCellValueFactory(new PropertyValueFactory<Planning, Integer>("dureeTraitement"));
        col_dejeuner.setCellValueFactory(new PropertyValueFactory<Planning, String>("dejeuner"));
        col_nom1.setCellValueFactory(new PropertyValueFactory<Planning, String>("nomAgee"));
        col_petitDejeuner.setCellValueFactory(new PropertyValueFactory<Planning, String>("petitDejeuner"));
        col_desc_traitement.setCellValueFactory(new PropertyValueFactory<Planning, String>("desc_traitement"));
        col_prenom1.setCellValueFactory(new PropertyValueFactory<Planning, String>("prenomAgee"));
        col_id_Agee.setCellValueFactory(new PropertyValueFactory<Planning, Integer>("id_Agee"));
        col_diner.setCellValueFactory(new PropertyValueFactory<Planning, String>("diner"));
        col_medicament.setCellValueFactory(new PropertyValueFactory<Planning, String>("medicament"));





        col_supprimer2 = new TableColumn("Supprimer");
        //col_supprimer2.setCellValueFactory(new PropertyValueFactory<User, String>("supprimer"));

        Callback<TableColumn<Planning, String>, TableCell<Planning, String>> cellFactory2
                = //
                new Callback<TableColumn<Planning, String>, TableCell<Planning, String>>() {
                    @Override
                    public TableCell call(final TableColumn<Planning, String> param) {
                        final TableCell<Planning, String> cell = new TableCell<Planning, String>() {

                            final Button btn = new Button("supprimer");

                            @Override
                            public void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                if (empty) {
                                    setGraphic(null);
                                    setText(null);
                                } else {
                                    btn.setOnAction(event -> {
                                        Planning p = getTableView().getItems().get(getIndex());
                                        System.out.println(p.ToString());
//                                        ps.delete(p);

                                        Parent root;
                                        try {
                                            root= FXMLLoader.load(getClass().getResource("Home2.fxml"));
                                            btn.getScene().setRoot(root);
                                            pnlOrders.setStyle("-fx-background-color : #BBD2E1");
                                            pnlOrders.toFront();

                                            sup2 =true ;
                                            sup1 =false ;

                                        } catch (IOException ex) {
                                            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                        pnlOrders.setStyle("-fx-background-color : #BBD2E1");
                                        pnlOrders.toFront();

                                    });
                                    setGraphic(btn);
                                    setText(null);
                                }
                            }
                        };
                        return cell;
                    }
                };

        col_supprimer2.setCellFactory(cellFactory2);
        tableUser1.getColumns().add(col_supprimer2);









        editableCols();

    }

    private void editableCols(){
        col_nom.setCellFactory(TextFieldTableCell.forTableColumn());

        col_nom.setOnEditCommit(e->{
            e.getTableView().getItems().get(e.getTablePosition().getRow()).setNom(e.getNewValue());
            final int row =  tableUser.getEditingCell().getRow();
            System.out.println("_______________"+row);
            String nom = tableUser.getItems().get(row).getNom();
            int id =  tableUser.getItems().get(row).getId();
            Admin_service as = new Admin_service();
            as.updateNom( id , nom);
        });

        col_prenom.setCellFactory(TextFieldTableCell.forTableColumn());

        col_prenom.setOnEditCommit(e->{
            e.getTableView().getItems().get(e.getTablePosition().getRow()).setPrenom(e.getNewValue());
            final int row =  tableUser.getEditingCell().getRow();
            System.out.println("_______________"+row);
            String prenom = tableUser.getItems().get(row).getPrenom();
            int id =  tableUser.getItems().get(row).getId();
            Admin_service as = new Admin_service();
            as.updatePrenom( id , prenom);
        });

        col_rib.setCellFactory(TextFieldTableCell.forTableColumn());

        col_rib.setOnEditCommit(e->{
            e.getTableView().getItems().get(e.getTablePosition().getRow()).setRib(e.getNewValue());
            final int row =  tableUser.getEditingCell().getRow();
            System.out.println("_______________"+row);
            String rib = tableUser.getItems().get(row).getRib();
            int id =  tableUser.getItems().get(row).getId();
            Admin_service as = new Admin_service();
            as.updateRib( id , rib);
        });

        col_cin.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));

        col_cin.setOnEditCommit(e->{
            e.getTableView().getItems().get(e.getTablePosition().getRow()).setCin(e.getNewValue());
            final int row =  tableUser.getEditingCell().getRow();
            System.out.println("_______________"+row);
            int cin = tableUser.getItems().get(row).getCin();
            int id =  tableUser.getItems().get(row).getId();
            Admin_service as = new Admin_service();
            as.updateCin( id , cin);
        });

        col_tel.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));

        col_tel.setOnEditCommit(e->{
            e.getTableView().getItems().get(e.getTablePosition().getRow()).setTelephone(e.getNewValue());
            final int row =  tableUser.getEditingCell().getRow();
            System.out.println("_______________"+row);
            int tel = tableUser.getItems().get(row).getTelephone();
            int id =  tableUser.getItems().get(row).getId();
            Admin_service as = new Admin_service();
            as.updateTelephone( id , tel);
        });


        col_adresse.setCellFactory(TextFieldTableCell.forTableColumn());

        col_adresse.setOnEditCommit(e->{
            e.getTableView().getItems().get(e.getTablePosition().getRow()).setAdresse(e.getNewValue());
            final int row =  tableUser.getEditingCell().getRow();
            System.out.println("_______________"+row);
            String adresse = tableUser.getItems().get(row).getAdresse();
            int id =  tableUser.getItems().get(row).getId();
            Admin_service as = new Admin_service();
            as.updateAdresse( id , adresse);
        });





//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MMM-dd");
//
//
//        col_date.setCellFactory(TextFieldTableCell.forTableColumn());
//
//        col_date.setOnEditCommit(e->{
//            try {
//                e.getTableView().getItems().get(e.getTablePosition().getRow()).setDateNaissance(formatter.parse(e.getNewValue()));
//            } catch (ParseException e1) {
//                e1.printStackTrace();
//            }
//            final int row =  tableUser.getEditingCell().getRow();
//            System.out.println("_______________"+row);
//            java.util.Date date = tableUser.getItems().get(row).getDateNaissance();
//            int id =  tableUser.getItems().get(row).getId();
//            Admin_service as = new Admin_service();
//            as.updateDate( id , date);
//        });




        col_desc_nutrition.setCellFactory(TextFieldTableCell.forTableColumn());

        col_desc_nutrition.setOnEditCommit(e->{
            e.getTableView().getItems().get(e.getTablePosition().getRow()).setDesc_nutrition(e.getNewValue());
            final int row =  tableUser1.getEditingCell().getRow();
            System.out.println("_______________"+row);
            String adresse = tableUser1.getItems().get(row).getDesc_nutrition();
            System.out.println(adresse);
            int id =  tableUser1.getItems().get(row).getid_Agee();
            Planning_service pl = new Planning_service();
            pl.updateDescNutrition( id , adresse);
        });


        col_medicament.setCellFactory(TextFieldTableCell.forTableColumn());

        col_medicament.setOnEditCommit(e->{
            e.getTableView().getItems().get(e.getTablePosition().getRow()).setMedicament(e.getNewValue());
            final int row =  tableUser1.getEditingCell().getRow();
            System.out.println("_______________"+row);
            String medicament = tableUser1.getItems().get(row).getMedicament();
            System.out.println(medicament);
            int id =  tableUser1.getItems().get(row).getid_Agee();
            Planning_service pl = new Planning_service();
            pl.updateNomMedicament( id , medicament);
        });


        col_desc_traitement.setCellFactory(TextFieldTableCell.forTableColumn());

        col_desc_traitement.setOnEditCommit(e->{
            e.getTableView().getItems().get(e.getTablePosition().getRow()).setDesc_traitement(e.getNewValue());
            final int row =  tableUser1.getEditingCell().getRow();
            System.out.println("_______________"+row);
            String traitement = tableUser1.getItems().get(row).getDesc_traitement();
            System.out.println(traitement);
            int id =  tableUser1.getItems().get(row).getid_Agee();
            Planning_service pl = new Planning_service();
            pl.updateTraitement( id , traitement);
        });

        col_petitDejeuner.setCellFactory(TextFieldTableCell.forTableColumn());

        col_petitDejeuner.setOnEditCommit(e->{
            e.getTableView().getItems().get(e.getTablePosition().getRow()).setPetitDejeuner(e.getNewValue());
            final int row =  tableUser1.getEditingCell().getRow();
            System.out.println("_______________"+row);
            String avantApresNon = tableUser1.getItems().get(row).getPetitDejeuner();
            System.out.println(avantApresNon);
            int id =  tableUser1.getItems().get(row).getid_Agee();
            Planning_service pl = new Planning_service();
            pl.updatePetitDejeuner( id , avantApresNon);
        });


        col_dejeuner.setCellFactory(TextFieldTableCell.forTableColumn());

        col_dejeuner.setOnEditCommit(e->{
            e.getTableView().getItems().get(e.getTablePosition().getRow()).setDejeuner(e.getNewValue());
            final int row =  tableUser1.getEditingCell().getRow();
            System.out.println("_______________"+row);
            String avantApresNon = tableUser1.getItems().get(row).getDejeuner();
            System.out.println(avantApresNon);
            int id =  tableUser1.getItems().get(row).getid_Agee();
            Planning_service pl = new Planning_service();
            pl.updatedejeuner( id , avantApresNon);
        });


        col_diner.setCellFactory(TextFieldTableCell.forTableColumn());

        col_diner.setOnEditCommit(e->{
            e.getTableView().getItems().get(e.getTablePosition().getRow()).setDiner(e.getNewValue());
            final int row =  tableUser1.getEditingCell().getRow();
            System.out.println("_______________"+row);
            String avantApresNon = tableUser1.getItems().get(row).getDiner();
            System.out.println(avantApresNon);
            int id =  tableUser1.getItems().get(row).getid_Agee();
            Planning_service pl = new Planning_service();
            pl.updateDiner( id , avantApresNon);
        });








        tableUser.setEditable(true);
        tableUser1.setEditable(true);






        // table Gestion Planning
//        col_nom1.setCellFactory(TextFieldTableCell.forTableColumn());
//
//        col_nom1.setOnEditCommit(e->{
//            e.getTableView().getItems().get(e.getTablePosition().getRow()).setNom(e.getNewValue());
//            final int row =  tableUser.getEditingCell().getRow();
//            System.out.println("_______________"+row);
//            String nom = tableUser.getItems().get(row).getNom();
//            int id =  tableUser.getItems().get(row).getId();
//            Admin_service as = new Admin_service();
//            as.updateNom( id , nom);
//        });


    }









    public boolean isInteger(String string) {
        try {
            Integer.valueOf(string);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public boolean isDate(String string) {
        try {
            Date.valueOf(string);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    public void handleClicks(ActionEvent actionEvent) {
        if (actionEvent.getSource() == btnCustomers) {
//            pnlCustomer.setStyle("-fx-background-color : #BBD2E1");
            pnlCustomer.setStyle("-fx-background-color : #BBD2E1");
            pnlCustomer.toFront();
        }
        if (actionEvent.getSource() == btnMenus) {
            pnlMenus.setStyle("-fx-background-color : #53639F");
            pnlMenus.toFront();
        }
        if (actionEvent.getSource() == btnOverview) {
            Parent root;
            try {
                root= FXMLLoader.load(getClass().getResource("Home2.fxml"));
                btnOverview.getScene().setRoot(root);
            } catch (IOException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }

            pnlOverview.setStyle("-fx-background-color : #BBD2E1");
            pnlOverview.toFront();

        }

        if(actionEvent.getSource()==btnOrders)
        {
//            pnlOrders.setStyle("-fx-background-color : #464F67");
            pnlOrders.setStyle("-fx-background-color : #BBD2E1");
            pnlOrders.toFront();
        }
        if(actionEvent.getSource()==col_supprimer2)
        {
//            pnlOrders.setStyle("-fx-background-color : #464F67");
            pnlOrders.setStyle("-fx-background-color : #BBD2E1");
            pnlOrders.toFront();
        }


        if(actionEvent.getSource()==btn_ajouter)
        {
            boolean ok = true ;


            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setHeaderText(null);


            User agee = new Agee();

            if(tf_nom.getText().contains("'")||tf_nom.getText().contains("\"")||tf_nom.getText().length()>30){
                tf_nom.setText("");
                ok = false ;
            }else{
                agee.setNom(tf_nom.getText());
            }
            if(tf_prenom.getText().contains("'")||tf_prenom.getText().contains("\"")||tf_prenom.getText().length()>30){
                tf_prenom.setText("");
                ok = false ;
            }else{
                agee.setPrenom(tf_prenom.getText());
            }
            if(tf_mail.getText().contains("'")||tf_mail.getText().contains("\"")||tf_mail.getText().length()>50||!(tf_mail.getText().contains("@"))){
                tf_mail.setText("");
            }else{
                agee.setMail(tf_mail.getText());
            }
            if(tf_region.getText().contains("'")||tf_region.getText().contains("\"")||tf_region.getText().length()>40){
                tf_region.setText("");
            }else{
                agee.setRegion(tf_region.getText());
            }
            if(tf_adresse.getText().contains("'")||tf_adresse.getText().contains("\"")||tf_adresse.getText().length()>40){
                tf_adresse.setText("");
            }else{
                agee.setAdresse(tf_adresse.getText());
            }
            if( (!isInteger(tf_telephone.getText())) || tf_telephone.getText().length()>20){
                tf_telephone.setText("");
                ok = false ;
            }else{
                agee.setTelephone(Integer.parseInt(tf_telephone.getText()));
            }
            if( (!isInteger(tf_cin.getText())) || tf_cin.getText().length()>20){
                tf_cin.setText("");
                ok = false ;
            }else{
                agee.setCin(Integer.parseInt(tf_cin.getText()));
            }
            if(tf_rib.getText().contains("'")||tf_rib.getText().contains("\"")||tf_rib.getText().length()>30){
                tf_rib.setText("");
                ok = false ;
            }else{
                agee.setRib(tf_rib.getText());
            }
            if(!rd_homme.isSelected()){
                agee.setSexe("Homme");
            }else {
                agee.setSexe("Femme");
            }


            if (ok){
                as.insertPStatement(agee);
                alert.setTitle("Succès");
                alert.setContentText("Vous avez ajouter avec succès un Agée");

                alert.showAndWait();
            }else{
                alert.setTitle("Echec");
                alert.setContentText("champs invalides ou manquants");

                alert.showAndWait();

            }













        }


    }

    public void onEditChanged(TableColumn.CellEditEvent<User, String> userStringCellEditEvent) {


    }




    //______________________ Ajou d'un agé ____________________________________













}
