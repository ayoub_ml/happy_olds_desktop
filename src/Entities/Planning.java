/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

/**
 *
 * @author autonome
 */
public class Planning {

    String nomAgee ;
    String prenomAgee;

    private int id_pn ;
    private int id_ptm ;
    private int id_pvm ;
    private int id_Agee ;


    private String desc_nutrition ;

    private String medicament ;
    private String desc_traitement ;
    private int dureeTraitement ;
    private String petitDejeuner ;
    private String dejeuner ;
    private String diner ;

    private String date_rdv ;


    public Planning() {
    }


    public Planning(int id_pn, int id_ptm, int id_pvm ,  int id_Agee, String nomAgee , String prenomAgee , String desc_nutrition , String medicament , String desc_traitement, int dureeTraitement , String petitDejeuner , String dejeuner , String diner , String date_rdv) {
        this.id_pn = id_pn;
        this.id_ptm = id_ptm;
        this.id_pvm = id_pvm;
        this.id_Agee = id_Agee;
        this.nomAgee = nomAgee;
        this.prenomAgee = prenomAgee;
        this.desc_nutrition = desc_nutrition;
        this.medicament = medicament;
        this.desc_traitement = desc_traitement;
        this.dureeTraitement = dureeTraitement;
        this.petitDejeuner = petitDejeuner;
        this.dejeuner = dejeuner;
        this.diner = diner;
        this.date_rdv = date_rdv;

        this.ToString();
    }


    

    public Planning(int id_pn, int id_ptm,  int id_Agee, String desc) {
        this.id_pn = id_pn;
        this.id_ptm = id_ptm;
        this.id_Agee = id_Agee;
        this.desc_nutrition = desc;
    }
    
    
    public String ToString(){
        return "_____________________________________________________Planning____________________________"+
                "nom :"+nomAgee
                +"\n prenom :"+prenomAgee;
    }


    public String getNomAgee() {
        return nomAgee;
    }

    public String getPrenomAgee() {
        return prenomAgee;
    }

    public int getId_pn() {
        return id_pn;
    }

    public int getId_ptm() {
        return id_ptm;
    }

    public int getId_pvm() {
        return id_pvm;
    }

    public int getid_Agee() {
        return id_Agee;
    }

    public String getDesc_nutrition() {
        return desc_nutrition;
    }

    public String getMedicament() {
        return medicament;
    }

    public String getDesc_traitement() {
        return desc_traitement;
    }

    public int getDureeTraitement() {
        return dureeTraitement;
    }

    public String getPetitDejeuner() {
        return petitDejeuner;
    }

    public String getDejeuner() {
        return dejeuner;
    }

    public String getDiner() {
        return diner;
    }

    public String getDate_rdv() {
        return date_rdv;
    }


    public void setNomAgee(String nomAgee) {
        this.nomAgee = nomAgee;
    }

    public void setPrenomAgee(String prenomAgee) {
        this.prenomAgee = prenomAgee;
    }

    public void setId_pn(int id_pn) {
        this.id_pn = id_pn;
    }

    public void setId_ptm(int id_ptm) {
        this.id_ptm = id_ptm;
    }

    public void setId_pvm(int id_pvm) {
        this.id_pvm = id_pvm;
    }

    public void setid_Agee(int id_Agee) {
        this.id_Agee = id_Agee;
    }

    public void setDesc_nutrition(String desc_nutrition) {
        this.desc_nutrition = desc_nutrition;
    }

    public void setMedicament(String medicament) {
        this.medicament = medicament;
    }

    public void setDesc_traitement(String desc_traitement) {
        this.desc_traitement = desc_traitement;
    }

    public void setDureeTraitement(int dureeTraitement) {
        this.dureeTraitement = dureeTraitement;
    }

    public void setPetitDejeuner(String petitDejeuner) {
        this.petitDejeuner = petitDejeuner;
    }

    public void setDejeuner(String dejeuner) {
        this.dejeuner = dejeuner;
    }

    public void setDiner(String diner) {
        this.diner = diner;
    }

    public void setDate_rdv(String date_rdv) {
        this.date_rdv = date_rdv;
    }
}
