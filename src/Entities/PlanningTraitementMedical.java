/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

/**
 *
 * @author autonome
 */
public class PlanningTraitementMedical extends Planning {
    
    private String desc ;
    private String medicament ;
    private int dureeTraitement ;
    private String petitDejeuner ;
    private String dejeuner ;
    private String diner ;

    public PlanningTraitementMedical(String desc ) {
        this.desc = desc;
    }

    public PlanningTraitementMedical(String desc, String medicament, int dureeTraitement, String petitDejeuner, String dejeuner, String diner) {
        this.desc = desc;
        this.medicament = medicament;
        this.dureeTraitement = dureeTraitement;
        this.petitDejeuner = petitDejeuner;
        this.dejeuner = dejeuner;
        this.diner = diner;
    }
    
    

    public PlanningTraitementMedical(String desc, String medicament, int dureeTraitement) {
        this.desc = desc;
        this.medicament = medicament;
        this.dureeTraitement = dureeTraitement;
    }
    
    

    public PlanningTraitementMedical( int id_ptm , int id_Agee ,  String medicament , String desc , int dureeTraitement) {
        super(-1, id_ptm, id_Agee, desc);
        this.desc = desc;
        this.medicament = medicament;
        this.dureeTraitement = dureeTraitement;
    }
    
    

    public PlanningTraitementMedical(int aInt, int aInt0, String string) {
        super(-1, aInt,aInt0,string);
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public int getDureeTraitement() {
        return dureeTraitement;
    }

    public String getMedicament() {
        return medicament;
    }

    public void setDureeTraitement(int dureeTraitement) {
        this.dureeTraitement = dureeTraitement;
    }

    public void setMedicament(String medicament) {
        this.medicament = medicament;
    }

    
    
//    public boolean testBinaire(int i){
//        return(i == 0 || i == 1);
//   }
//
//    public void setDejeuner(int dejeuner) throws ExceptionBinaire{
//        if (testBinaire(dejeuner)){
//                    this.dejeuner = dejeuner;
//        }
//        else{
//            throw new ExceptionBinaire (dejeuner);
//        }
//
//    }
//
//    public void setDiner(int diner) throws ExceptionBinaire {
//        if (testBinaire(diner)){
//                    this.diner = diner;
//        }
//        else{
//            throw new ExceptionBinaire (diner);
//
//        }
//    }
//
//    public void setPetitDejeuner(int petitDejeuner) throws ExceptionBinaire {
//        if (testBinaire(petitDejeuner)){
//                    this.petitDejeuner = petitDejeuner;
//        }
//        else{
//            throw new ExceptionBinaire (petitDejeuner);
//
//        }
//    }

    public String getDejeuner() {
        return dejeuner;
    }

    public String getDiner() {
        return diner;
    }

    public String getPetitDejeuner() {
        return petitDejeuner;
    }
    
    
    
    
     
    
}
