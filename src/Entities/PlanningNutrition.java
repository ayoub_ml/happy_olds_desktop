/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

/**
 *
 * @author autonome
 */
public class PlanningNutrition extends Planning{
    private String desc ;

    public PlanningNutrition(String desc) {
        this.desc = desc;
    }

    public PlanningNutrition(int id_pn, int id_Agee, String desc) {
        super(id_pn, -1 ,id_Agee ,desc);
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
    
    
}
