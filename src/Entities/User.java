package Entities;

import javafx.scene.control.Button;

import java.util.Date;
import java.util.Objects;

enum type {
    agee,oldsitter,doctor,benevole
}
public class User {
    protected Button supprimer = new Button("Supprimer");

    protected int id;
    protected int cin;
    protected String nom;
    protected String prenom;
    protected Date dateNaissance;
    protected String mail;
    protected String pwd;
    protected String sexe;
    protected String region;
    protected String adresse;
    protected String rib;
    protected int telephone;
    protected String photo;
    protected type type;
    protected Double salaire;

    // a changer avec ses getters and setters
    protected String t ;

    public User() {
    }
    
    public User(String nom,String prenom, Date d ,String sexe,String mail,String region,String adresse,String pwd,int cin,int telephone , String rib) {
        this.nom=nom;
        this.prenom=prenom;
        this.dateNaissance= new java.sql.Date(2019-03-01) ;
        this.sexe=sexe;
        this.mail=mail;
        this.region=region;
        this.adresse=adresse;
        this.pwd=pwd;
        this.cin=cin;
        this.telephone=telephone;
        this.rib=rib ;

        t = "agee" ;
    }


    public User(int id , String nom,String prenom, Date d ,String sexe,String mail,String region,String adresse,String pwd,int cin,int telephone , String rib) {
        this.id = id ;
        this.nom=nom;
        this.prenom=prenom;
        this.dateNaissance= new java.sql.Date(2019-03-01) ;
        this.sexe=sexe;
        this.mail=mail;
        this.region=region;
        this.adresse=adresse;
        this.pwd=pwd;
        this.cin=cin;
        this.telephone=telephone;
        this.rib=rib ;


        t = "agee" ;

        System.out.println(this.toString());
    }
    
    
    
    public User(String nom, String prenom) {
        this.nom=nom;
        this.prenom=prenom;
    }

    // cette methode est appelé par le service user afin d ' instancier un user et de récuperer sont id , qui est générer par la base 
    public User(int id) {
        this.id = id;
    }
    
    public String getType(){
        return(t);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCin() {
        return cin;
    }

    public void setCin(int cin) {
        this.cin = cin;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(java.util.Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public int getTelephone() {
        return telephone;
    }

    public void setTelephone(int telephone) {
        this.telephone = telephone;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String gettype() {
        return t;
    }

    public void setType(Entities.type type) {
        this.type = type;
    }

    public Double getSalaire() {
        return salaire;
    }

    public void setSalaire(Double salaire) {
        this.salaire = salaire;
    }

    public String getRib() {
        return rib;
    }

    public void setRib(String rib) {
        this.rib = rib;
    }

    public Button getSupprimer() {
        return supprimer;
    }

    public void setSupprimer(Button Supprime) {
        this.supprimer = Supprime;
    }




    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                cin == user.cin &&
                telephone == user.telephone &&
                Objects.equals(nom, user.nom) &&
                Objects.equals(prenom, user.prenom) &&
                Objects.equals(dateNaissance, user.dateNaissance) &&
                Objects.equals(mail, user.mail) &&
                Objects.equals(pwd, user.pwd) &&
                Objects.equals(sexe, user.sexe) &&
                Objects.equals(region, user.region) &&
                Objects.equals(adresse, user.adresse) &&
                Objects.equals(photo, user.photo) &&
                type == user.type &&
                Objects.equals(salaire, user.salaire);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, cin, nom, prenom, dateNaissance, mail, pwd, sexe, region, adresse, telephone, photo, type, salaire);
    }

    @Override
    public String toString() {
        return "\n \n \n \n \nUser{" +
                "id=" + id +
                "\n, cin=" + cin +
                "\n, rib=" + rib +
                "\n, nom='" + nom + '\'' +
                "\n, prenom='" + prenom + '\'' +
                "\n, dateNaissance=" + dateNaissance +
                "\n, mail='" + mail + '\'' +
                "\n, pwd='" + pwd + '\'' +
                "\n, sexe='" + sexe + '\'' +
                "\n, region='" + region + '\'' +
                "\n, adresse='" + adresse + '\'' +
                "\n, telephone=" + telephone +
                "\n, photo='" + photo + '\'' +
                "\n, type=" + type +
                "\n, salaire=" + salaire + 
                '}';
    }

    public User(int id,int cin,String nom,String prenom,Date dateNaissance,String mail,String pwd,String sexe, String region,String adresse,int telephone,String photo,type type,Double salaire) {
        this.id = id;
        this.cin = cin;
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
        this.mail = mail;
        this.pwd = pwd;
        this.sexe = sexe;
        this.region = region;
        this.adresse = adresse;
        this.telephone = telephone;
        this.photo = photo;
        this.type = type;
        this.salaire = salaire;
    }


}
